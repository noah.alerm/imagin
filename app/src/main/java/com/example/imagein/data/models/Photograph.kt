package com.example.imagein.data.models

data class Photograph(val id: Int, var author: String, var width: Int, var height: Int, var url: String,
                      var download_url: String, var favorite: Boolean = false)
