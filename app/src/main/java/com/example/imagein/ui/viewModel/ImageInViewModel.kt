package com.example.imagein.ui.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.imagein.data.models.Photograph
import com.example.imagein.data.repository.Repository
import com.example.imagein.database.PhotoApplication
import com.example.imagein.database.entities.PhotoEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ImageInViewModel : ViewModel() {
    //ATTRIBUTES
    private var photographs = mutableListOf<Photograph>()
    private var livePhotographs = MutableLiveData<List<Photograph>>()
    var searchedCharacters: String = ""

    //BOOLEANS
    //list
    var isFirstTime = true
    var comesFromFavorites = false
    var comesFromSearch = false
    //detail
    var comesFromDetail = false
    //settings
    var settingsEnter = true
    var darkModeOn = false
    var vibrationOn = true
    var soundOn = true

    //INIT
    init {
        //There are 34 pages of photographs on the API
        for (i in 1..34) {
            viewModelScope.launch {
                val response = withContext(Dispatchers.IO) { Repository().getPhotographs(i) }

                if (response.isSuccessful) {
                    //The data is collected here and then added to the MutableLiveData as otherwise only one page is added to the recyclerview.
                    photographs.addAll(response.body()!!.toMutableList())

                    CoroutineScope(Dispatchers.Main).launch {
                        val photoList = withContext(Dispatchers.IO) { PhotoApplication.database.photoDao().getAllPhotos() }
                        Log.d("DATABASE", photoList.toString())

                        for (photo in photographs) {
                            val picture = PhotoEntity(photo.id)

                            if (photoList.contains(picture)) {
                                photo.favorite = true
                            }
                        }
                    }
                }
                else {
                    Log.e("ERROR", response.message())
                }
            }
        }

        //Collection of all pages into the MutableLiveData.
        livePhotographs.postValue(photographs)
    }

    //GETTER
    fun getPhotographs() = livePhotographs
}
