package com.example.imagein.ui.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.TypedValue
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.MenuRes
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.imagein.ui.viewModel.ImageInViewModel
import com.example.imagein.R
import com.example.imagein.ui.adapters.ListAdapter
import com.google.android.material.chip.Chip

class ListFragment : Fragment(R.layout.fragment_list) {
    //ATTRIBUTES
    //recycler view
    private lateinit var recyclerView: RecyclerView
    //icons
    private lateinit var goBackIcon: ImageView
    private lateinit var menuIcon: ImageView
    private lateinit var favoritesIcon: ImageView
    private lateinit var searchIcon: ImageView
    //chips
    private lateinit var mainPageChip: Chip
    private lateinit var favoritesChip: Chip
    private lateinit var searchChip: Chip

    //view model
    private val viewModel: ImageInViewModel by activityViewModels()

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //IDs
        recyclerView = view.findViewById(R.id.recycler_view_list)
        goBackIcon = view.findViewById(R.id.go_back)
        menuIcon = view.findViewById(R.id.menu_icon)
        favoritesIcon = view.findViewById(R.id.favorites_section_icon)
        searchIcon = view.findViewById(R.id.search_icon)
        mainPageChip = view.findViewById(R.id.main_page_chip)
        favoritesChip = view.findViewById(R.id.favorites_chip)
        searchChip = view.findViewById(R.id.search_chip)

        //RECYCLER VIEW
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        //ADAPTER AND ICON VISIBILITY MANAGEMENT
        when {
            //FROM FAVORITES ITEM TO FAVORITES
            viewModel.comesFromFavorites && !viewModel.comesFromSearch -> {
                val adapter = ListAdapter(requireContext(), true, viewModel)
                recyclerView.adapter = adapter
                viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it.filter { it.favorite }
                    .toMutableList())}) //LIVE DATA

                goBackIcon.visibility = View.VISIBLE
                favoritesChip.visibility = View.VISIBLE
            }
            //FROM SEARCHED FAVORITES ITEM TO SEARCHED FAVORITES
            viewModel.comesFromFavorites && viewModel.comesFromSearch -> {
                getFilteredList(viewModel.searchedCharacters)

                goBackIcon.visibility = View.VISIBLE
                favoritesChip.visibility = View.VISIBLE
                searchChip.visibility = View.VISIBLE
            }
            //FROM SEARCHED ITEM TO SEARCHED LIST
            !viewModel.comesFromFavorites && viewModel.comesFromSearch -> {
                getFilteredList(viewModel.searchedCharacters)

                goBackIcon.visibility = View.VISIBLE
                mainPageChip.visibility = View.VISIBLE
                searchChip.visibility = View.VISIBLE
            }
            //MAIN PAGE
            else -> {
                getNormalList()

                mainPageChip.visibility = View.VISIBLE
            }
        }


        //ON CLICK
        //favorites
        favoritesIcon.setOnClickListener {
            //ANIMATION
            YoYo.with(Techniques.RotateIn).duration(300).playOn(favoritesIcon)

            //FAVORITES ADAPTER
            val adapter = ListAdapter(requireContext(), true, viewModel)
            recyclerView.adapter = adapter
            viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it.filter { it.favorite }
                .toMutableList())}) //LIVE DATA

            //ICON AND BOOLEAN MANAGEMENT
            goBackIcon.visibility = View.VISIBLE
            viewModel.comesFromFavorites = true
            viewModel.comesFromSearch = false
            mainPageChip.visibility = View.GONE
            favoritesChip.visibility = View.VISIBLE
            searchChip.visibility = View.GONE
        }

        //go back
        goBackIcon.setOnClickListener {
            //FROM SEARCHED FAVORITES TO FAVORITES
            if (viewModel.comesFromSearch && viewModel.comesFromFavorites) {
                val adapter = ListAdapter(requireContext(), true, viewModel)
                recyclerView.adapter = adapter
                viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it.filter { it.favorite }
                    .toMutableList())}) //LIVE DATA

                viewModel.comesFromSearch = false
                searchChip.visibility = View.GONE

                //ANIMATION
                YoYo.with(Techniques.RotateIn).duration(300).playOn(searchIcon)
            }
            //TO MAIN PAGE
            else {
                getNormalList()

                //ANIMATION
                //search
                if (viewModel.comesFromSearch)
                    YoYo.with(Techniques.RotateIn).duration(300).playOn(searchIcon)
                //favorites
                else
                    YoYo.with(Techniques.RotateIn).duration(300).playOn(favoritesIcon)

                viewModel.comesFromSearch = false
                viewModel.comesFromFavorites = false
                goBackIcon.visibility = View.GONE
                mainPageChip.visibility = View.VISIBLE
                favoritesChip.visibility = View.GONE
                searchChip.visibility = View.GONE
            }
        }

        //search
        searchIcon.setOnClickListener {
            //ANIMATION
            YoYo.with(Techniques.RotateIn).duration(300).playOn(searchIcon)

            //CUSTOM TITLE
            val title = TextView(requireContext())
            title.text = "SEARCH"
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
            title.gravity = Gravity.CENTER
            title.setTextColor(ContextCompat.getColor(requireContext(), R.color.logo_pink))
            title.typeface = Typeface.DEFAULT_BOLD
            title.background = getDrawable(requireContext(), R.drawable.search_dialog_title_bg)
            title.setPadding(0, 30, 0, 30)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.SearchDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_search_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(title)
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("SEARCH") { _, _ ->
                //ATTRIBUTES
                val textInput: EditText = viewDialog.findViewById(R.id.search_input)

                //SEARCH
                if (textInput.text.toString() != "") {
                    getFilteredList(textInput.text.toString())
                    viewModel.searchedCharacters = textInput.text.toString()
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //menu
        menuIcon.setOnClickListener { v: View ->
            YoYo.with(Techniques.Wave).playOn(menuIcon)
            showMenu(v, R.menu.popup_menu)
        }
    }

    //METHODS
    /**
     * This method is used to set the recyclerview as the normal list.
     */
    private fun getNormalList() {
        //The first time the user opens the app, the recyclerview needs some time to load all the items.
        if (viewModel.isFirstTime) {
            viewModel.isFirstTime = false

            Handler(Looper.getMainLooper()).postDelayed({
                val adapter = ListAdapter(requireContext(), false, viewModel)
                recyclerView.adapter = adapter
                viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it)}) //LIVE DATA
            }, 700)
        }
        else {
            val adapter = ListAdapter(requireContext(), false, viewModel)
            recyclerView.adapter = adapter
            viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it)}) //LIVE DATA
        }
    }

    /**
     * This method is used to set the recycler view as a filtered list through the search button.
     * @param characters Characters to search
     */
    @SuppressLint("SetTextI18n")
    private fun getFilteredList(characters: String) {
        //If the user searches in the favorites section, only liked photographs containing the entered characters will appear.
        if (viewModel.comesFromFavorites) {
            val adapter = ListAdapter(requireContext(), true, viewModel)
            recyclerView.adapter = adapter
            viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it.filter { it.favorite &&
                    (it.author.lowercase().contains(characters.lowercase()) || it.id.toString().contains(characters)) }
                .toMutableList())}) //LIVE DATA
        }
        //If the user searches in the main page, all photographs containing the entered characters will appear.
        else{
            val adapter = ListAdapter(requireContext(), false, viewModel)
            recyclerView.adapter = adapter
            viewModel.getPhotographs().observe(viewLifecycleOwner, { adapter.setPhotographs(it.filter {
                it.author.lowercase().contains(characters.lowercase()) || it.id.toString().contains(characters) }
                .toMutableList())}) //LIVE DATA
        }

        goBackIcon.visibility = View.VISIBLE
        searchChip.visibility = View.VISIBLE
        searchChip.text = "SEARCH: $characters"
        viewModel.comesFromSearch = true
    }

    /**
     * This method is used to show the popup menu.
     * @param v View
     * @param menuRes Menu resource (INT, e.g. R.menu.popup_menu)
     */
    private fun showMenu(v: View, @MenuRes menuRes: Int) {
        val popup = PopupMenu(context!!, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { item: MenuItem ->
            when (item.itemId) {
                R.id.settings -> findNavController().navigate(R.id.action_list_to_settings)
                R.id.help -> showHelp()
                R.id.API -> showAPIInfo()
            }
            true
        }
        popup.show()
    }

    //MENU OPTIONS
    @SuppressLint("SetTextI18n")
    /**
     * This method is used to show the help dialog.
     */
    private fun showHelp() {
        //CUSTOM TITLE
        val title = TextView(requireContext())
        title.text = "HELP"
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
        title.gravity = Gravity.CENTER
        title.setTextColor(ContextCompat.getColor(requireContext(), R.color.logo_pink))
        title.typeface = Typeface.DEFAULT_BOLD
        title.background = getDrawable(requireContext(), R.drawable.search_dialog_title_bg)
        title.setPadding(0, 30, 0, 30)

        //CUSTOM DIALOG
        val builder = AlertDialog.Builder(requireContext(), R.style.SearchDialog)
        val viewDialog = layoutInflater.inflate(R.layout.custom_help_dialog, null)
        builder.setView(viewDialog)
        builder.setCustomTitle(title)
        builder.setPositiveButton("OK", null)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        dialog.show()
    }

    /**
     * This method is used to show the API Info dialog.
     */
    @SuppressLint("SetTextI18n")
    private fun showAPIInfo() {
        //CUSTOM TITLE
        val title = TextView(requireContext())
        title.text = "API Info"
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
        title.gravity = Gravity.CENTER
        title.setTextColor(ContextCompat.getColor(requireContext(), R.color.logo_pink))
        title.typeface = Typeface.DEFAULT_BOLD
        title.background = getDrawable(requireContext(), R.drawable.search_dialog_title_bg)
        title.setPadding(0, 30, 0, 30)

        //CUSTOM DIALOG
        val builder = AlertDialog.Builder(requireContext(), R.style.SearchDialog)
        val viewDialog = layoutInflater.inflate(R.layout.custom_api_dialog, null)
        builder.setView(viewDialog)
        builder.setCustomTitle(title)
        builder.setPositiveButton("OK", null)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
        dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        dialog.show()
    }
}
